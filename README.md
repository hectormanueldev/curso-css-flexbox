# Curso de CSS Flexbox

## Temario

1. :heavy_plus_sign: Display
1. :heavy_plus_sign: Flex direction
1. :heavy_plus_sign: Flex wrap
1. :heavy_plus_sign: Justify content
1. :heavy_plus_sign: Align items
1. :heavy_plus_sign: Align content
1. :heavy_plus_sign: Flex grow
1. :heavy_plus_sign: Flex shrink
1. :heavy_plus_sign: Flex basis
1. :heavy_plus_sign: Order y align self
1. :heavy_plus_sign: Flexbox grid bootstrap
1. :heavy_plus_sign: Flexbox grid artesanal
1. :heavy_plus_sign: Ejemplo uno flexbox artesanal
1. :heavy_plus_sign: Ejemplo dos flexbox artesanal
1. :heavy_plus_sign: Ejemplo dos flexbox bootstrap
